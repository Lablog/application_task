package com.vincent.applicationtask.controller;

import com.vincent.applicationtask.entity.Branch;
import com.vincent.applicationtask.entity.ErrorMessage;
import com.vincent.applicationtask.entity.Repository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

//Class for the controller to get the repository information for the requested user
@RestController
public class GitHubUserController {
    private static String gitHubURL = "https://api.github.com/users/";

    //request mapping for the path /application/json with the username value
    @RequestMapping(
            method = RequestMethod.GET,
            path = "application/json",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    //function for the request for the repositories of the given username
    public @ResponseBody Object getRepositoriesFromUser(@RequestParam(value="username") String username){
        try {
            //getting the HttpURLConnection for the given username to it's repositories of github
            HttpURLConnection connection = getConnection(gitHubURL + username + "/repos");

            int responseCode = connection.getResponseCode();

            //checking if the response code indicates an error
            if(responseCode > 299){
                ErrorMessage error =  responseErrorHandling(responseCode, connection);
                connection.disconnect();
                return error.toString();
            }

            //if nor error appeared, the input stream is read and put into this StringBuffer
            StringBuffer responseContent = getContent(connection);
            //the connection is disconnected
            connection.disconnect();
            //the content of the responses is read and parsed and then displayed
            return parseResponse(responseContent.toString());

        } catch (IOException e) {
            //IOException handling: it simply displays the error
            e.printStackTrace();
            return (new ErrorMessage(400, e.getMessage())).toString();
        }
    }

    //function to get the input stream from an HttpURLConnection
    private StringBuffer getContent(HttpURLConnection connection) throws IOException {
        StringBuffer responseContent = new StringBuffer();
        BufferedReader reader;
        String line;
        reader = new BufferedReader((new InputStreamReader(connection.getInputStream())));
        while ((line = reader.readLine()) != null) {
            responseContent.append(line);
        }
        reader.close();
        return responseContent;
    }
    //function to create an HttpURLConnection and connect it
    private HttpURLConnection getConnection(String path) throws IOException {
        URL url = new URL(path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);

        return connection;
    }

    /*function to return an object of the class ErrorMessage, if the response code of the http connection
        indicates some sort of error and return the information of that error
     */
    private ErrorMessage responseErrorHandling(int responseCode, HttpURLConnection connection) throws IOException {
        if(responseCode==404){
            return new ErrorMessage(404, "User does not exist at GitHub!");
        }else{
            StringBuilder responseContent = new StringBuilder();
            BufferedReader reader;
            String line;
            reader = new BufferedReader((new InputStreamReader(connection.getErrorStream())));
            while ((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            reader.close();
            return new ErrorMessage(responseCode, responseContent.toString());
        }

    }

    //ExceptionHandler if no username is given in the header as value
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ErrorMessage handleMissingParams(MissingServletRequestParameterException ex){
        String name = ex.getParameterName();
        int status = 400;
        String errMessage = name + " parameter is missing!";
        return new ErrorMessage(status, errMessage);
    }

    /*function to parse the entry of the response body into the corresponding objects and lastly in the list of
        repositories and then return the information formatted
     */
    private Object parseResponse(String responseBody) throws IOException {
        JSONArray responses = new JSONArray(responseBody);
        List<Repository> repositories = new ArrayList<>();
        for(int i = 0; i< responses.length(); i++){
            JSONObject repo = responses.getJSONObject(i);
            boolean fork = repo.getBoolean("fork");
            if(!fork){
                String name =  repo.getString("name");
                String ownerLogin = repo.getJSONObject("owner").getString("login");
                HttpURLConnection connection = getConnection(getBranchURL(repo));


                if(connection.getResponseCode() > 299){
                    ErrorMessage error =  responseErrorHandling(connection.getResponseCode(), connection);
                    connection.disconnect();
                    return error.toString();
                }
                StringBuffer responseContent = getContent(connection);
                connection.disconnect();
                List<Branch> branches = getBranches(responseContent.toString());
                repositories.add(new Repository(name, ownerLogin, branches));

            }
        }
        return createDisplayValue(repositories);
    }

    //function to get the url for the branches of the corresponding repository
    private String getBranchURL(JSONObject repo) {
        String branchURL = repo.getString("branches_url");
        return branchURL.substring(0, branchURL.length() - 9);
    }

    //function to return the information of the list of repositories in a better format
    private String createDisplayValue(List<Repository> repositories) {
        String display = "[\n";
        for(Repository repo:repositories){
            display = display.concat(repo.toString() + '\n');
        }
        display += "]";
        return display;
    }

    //function to get the list fo branches with all needed information from the corresponding response body
    private List<Branch> getBranches(String responseBody){
        JSONArray responses = new JSONArray(responseBody);
        List<Branch> branches = new ArrayList<>();
        for(int i = 0; i< responses.length(); i++){
            JSONObject branch = responses.getJSONObject(i);
            String name = branch.getString("name");
            String sha = branch.getJSONObject("commit").getString("sha");
            branches.add(new Branch(name, sha));

        }
        return branches;
    }
}
