package com.vincent.applicationtask.controller;

import com.vincent.applicationtask.entity.ErrorMessage;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//Class of the Controller for requesting a wrong format
@RestController
public class WrongFormatController {

    //RequestMapping for the path /application/xml, which is a wrong format
    @RequestMapping(
        method = RequestMethod.GET,
        path = "application/xml",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    //function for wrong format response
    public @ResponseBody String getWrongFormatError(){
        int status = 406;
        String errMessage = "Wrong format used! Please use JSON!";
        return (new ErrorMessage(status, errMessage)).toString();
    }
}
