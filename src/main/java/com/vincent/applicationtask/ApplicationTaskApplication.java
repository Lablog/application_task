package com.vincent.applicationtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationTaskApplication {
	//Main Function to start the Spring Boot Application
	public static void main(String[] args) {
		SpringApplication.run(ApplicationTaskApplication.class, args);
	}

}
