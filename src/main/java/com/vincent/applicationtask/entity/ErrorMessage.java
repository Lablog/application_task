package com.vincent.applicationtask.entity;

//Class for an errormessage if some error appeared with the error status and message
public class ErrorMessage {

    private int status;
    private String message;

    public ErrorMessage(int status, String message){
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    //overridden toString() function to give the function in a better format out
    @Override
    public String toString(){
        return "{\n" +
                "\t\"status\":" + this.status + '\n' +
                "\t\"Message\":" + this.message + '\n' +
                '}';
    }
}
