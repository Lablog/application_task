package com.vincent.applicationtask.entity;

//Class for a single branch in a repository with all needed information for this case
public class Branch {
    private String name;
    private String sha;

    public Branch(String name, String sha) {
        this.name = name;
        this.sha = sha;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    //overridden toString() function to give the function in a better format out
    @Override
    public String toString(){
        return "\t\t{\n" +
                "\t\t\t\"name\":" + this.name + '\n' +
                "\t\t\t\"sha\":" + this.sha + '\n' +
                "\t\t}";
    }
}
