package com.vincent.applicationtask.entity;

import java.util.List;

/* Class for a repository from Github with all needed information for this case
    The needed information are the name, the owner login and a list of branches
 */
public class Repository {
    private String name;
    private String ownerLogin;
    private List<Branch> branches;

    public Repository(String name, String ownerLogin, List<Branch> branches) {
        this.name = name;
        this.ownerLogin = ownerLogin;
        this.branches = branches;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerLogin() {
        return ownerLogin;
    }

    public void setOwnerLogin(String ownerLogin) {
        this.ownerLogin = ownerLogin;
    }

    public List<Branch> getBranches() {
        return branches;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }

    //overridden toString() function to give the function in a better format out
    @Override
    public String toString(){
        String value = "\t{\n" +
                        "\t\t\"name:\"" + this.name + '\n'+
                        "\t\t\"ownerlogin\"" + this.ownerLogin + '\n';
        for(Branch branch: branches){
            value = value.concat(branch.toString() + '\n');
        }
        value += "\t}";
        return value;
    }
}
